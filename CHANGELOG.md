## [1.0.1](https://gitlab.com/dreamer-labs/maniac/ansible_role_repositories/compare/v1.0.0...v1.0.1) (2021-01-22)


### Bug Fixes

* GPG Check set as default ([de3ab46](https://gitlab.com/dreamer-labs/maniac/ansible_role_repositories/commit/de3ab46))

# 1.0.0 (2020-02-06)


### Features

* Add ability to set repo priorities ([325171f](https://gitlab.com/dreamer-labs/maniac/ansible_role_repositories/commit/325171f))
